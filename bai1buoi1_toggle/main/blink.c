/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

/* Can use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO
#define BUTTON 5

int flag = 0;
int state = 0;

void app_main(void)
{
    gpio_pad_select_gpio(BLINK_GPIO);
    gpio_pad_select_gpio(BUTTON);   

    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_direction(BUTTON, GPIO_MODE_INPUT);

    gpio_set_pull_mode(BUTTON, GPIO_PULLUP_ONLY);

    gpio_set_level(BLINK_GPIO, 0);

    while(1) {
        if(gpio_get_level(BUTTON) == 0)
            flag = 1;

        if(gpio_get_level(BUTTON) == 1 && flag == 1)
        {
            flag = 0;
            state = (state == 0) ? 1 : 0;
            gpio_set_level(BLINK_GPIO, state);
            if (state == 1)
                printf("Turning on the LED\n");
            else
                printf("Turning off the LED\n");
        }
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

